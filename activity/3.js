db.fruits.aggregate([
    {$unwind: "$onSale"},
    {$group: {_id:"$onSale", fruits: {$sum: 1}}}
    ]
)